import React, { useState, useEffect } from 'react';

function RegistrationForm({ onRegister }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirm, setPasswordConfirm] = useState('');

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handlePasswordConfirmChange = (event) => {
    setPasswordConfirm(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    // Perform validation
    // ...

    // Create user object
    const user = {
      email: email,
      password: password
    };

    // Pass user object to parent component for registration
    onRegister(user);

    // Reset form fields
    setEmail('');
    setPassword('');
    setPasswordConfirm('');
  };

  return (
    <form onSubmit={handleSubmit}>
      <input type="email" value={email} onChange={handleEmailChange} placeholder="Email" required />
      <input type="password" value={password} onChange={handlePasswordChange} placeholder="Password" required />
      <input type="password" value={passwordConfirm} onChange={handlePasswordConfirmChange} placeholder="Confirm Password" required />
      <button type="submit">Register</button>
    </form>
  );
}

function LoginForm({ onLogin }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    // Perform login and validation
    // ...

    // Retrieve users from localStorage
    const usersJson = localStorage.getItem('users');
    if (usersJson) {
      const users = JSON.parse(usersJson);
      const user = users.find((u) => u.email === email && u.password === password);
      if (user) {
        onLogin(user);
      } else {
        alert('Invalid credentials');
      }
    } else {
      alert('User not found');
    }

    // Reset form fields
    setEmail('');
    setPassword('');
  };

  return (
    <form onSubmit={handleSubmit}>
      <input type="email" value={email} onChange={handleEmailChange} placeholder="Email" required />
      <input type="password" value={password} onChange={handlePasswordChange} placeholder="Password" required />
      <button type="submit">Login</button>
    </form>
  );
}

function UserForm() {
  const [users, setUsers] = useState([]);
  const [currentUser, setCurrentUser] = useState(null);
  const [editingUser, setEditingUser] = useState(null);

  const handleRegister = (user) => {
    // Add user to the list of users
    const updatedUsers = [...users, user];
    setUsers(updatedUsers);

    // Save updated user list to localStorage
    localStorage.setItem('users', JSON.stringify(updatedUsers));
  };

  const handleLogin = (user) => {
    setCurrentUser(user);
  };

  const handleLogout = () => {
    // Remove current user from state
    setCurrentUser(null);
  };

  const handleEditUser = (user) => {
    setEditingUser(user);
  };

  const handleSaveEdit = (updatedUser) => {
    // Update user in the list of users
    const updatedUsers = users.map((user) =>
      user.email === updatedUser.email ? { ...user, ...updatedUser } : user
    );
    setUsers(updatedUsers);

    // Save updated user list to localStorage
    localStorage.setItem('users', JSON.stringify(updatedUsers));

    // Clear editingUser state
    setEditingUser(null);
  };

  useEffect(() => {
    // Check if there are any users in localStorage
    const usersJson = localStorage.getItem('users');
    if (usersJson) {
      const savedUsers = JSON.parse(usersJson);
      setUsers(savedUsers);
    } else {
      // Nếu không có người dùng trong localStorage, set danh sách người dùng là mảng rỗng
      localStorage.setItem('users', JSON.stringify([]));
    }
  }, []);

  return (
    <div>
      {!currentUser && (
        <div>
          <h2>Registration</h2>
          <RegistrationForm onRegister={handleRegister} />
          <hr />
          <h2>Login</h2>
          <LoginForm onLogin={handleLogin} />
        </div>
      )}

      {currentUser && (
        <div>
          <p>Welcome, {currentUser.email}!</p>
          <button onClick={handleLogout}>Logout</button>
        </div>
      )}

      {editingUser ? (
        <div>
          <h2>Edit User</h2>
          <UserFormEdit user={editingUser} onSave={handleSaveEdit} />
        </div>
      ) : (
        <div>
          <h2>User List</h2>
          <UserList users={users} onEdit={handleEditUser} />
        </div>
      )}
    </div>
  );
}

function UserDetail({ user, onEdit }) {
  return (
    <div>
      <h2>User Detail</h2>
      <p>Avatar: {user.avatar}</p>
      <p>Email: {user.email}</p>
      <p>First Name: {user.firstName}</p>
      <p>Last Name: {user.lastName}</p>
      <p>Phone: {user.phone}</p>
      <p>Address: {user.address}</p>
      <p>CCCD: {user.cccd}</p>
      <p>Socials FB: {user.socialsFB}</p>
      <p>Socials TW: {user.socialsTW}</p>
      <button onClick={() => onEdit(user)}>Edit</button>
    </div>
  );
}

function UserFormEdit({ user, onSave }) {
  const [avatar, setAvatar] = useState(user.avatar);
  const [email, setEmail] = useState(user.email);
  const [firstName, setFirstName] = useState(user.firstName);
  const [lastName, setLastName] = useState(user.lastName);
  const [phone, setPhone] = useState(user.phone);
  const [address, setAddress] = useState(user.address);
  const [password, setPassword] = useState('');
  const [oldPassword, setOldPassword] = useState('');

  useEffect(() => {
    setOldPassword(user.password);
  }, [user.password]);

  const handleAvatarChange = (event) => {
    setAvatar(event.target.value);
  };

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handleFirstNameChange = (event) => {
    setFirstName(event.target.value);
  };

  const handleLastNameChange = (event) => {
    setLastName(event.target.value);
  };

  const handlePhoneChange = (event) => {
    setPhone(event.target.value);
  };

  const handleAddressChange = (event) => {
    setAddress(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleOldPasswordChange = (event) => {
    setOldPassword(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    // Tạo đối tượng người dùng đã được cập nhật
    const updatedUser = {
      avatar: avatar,
      email: email,
      firstName: firstName,
      lastName: lastName,
      phone: phone,
      address: address,
      password: password
    };

    // Pass updated user object to parent component for saving
    onSave(updatedUser);
  };

  return (
    <form onSubmit={handleSubmit}>
      <input type="text" value={avatar} onChange={handleAvatarChange} placeholder="Avatar" />
      <input type="email" value={email} onChange={handleEmailChange} placeholder="Email" required />
      <input type="text" value={firstName} onChange={handleFirstNameChange} placeholder="First Name" />
      <input type="text" value={lastName} onChange={handleLastNameChange} placeholder="Last Name" />
      <input type="text" value={phone} onChange={handlePhoneChange} placeholder="Phone" />
      <input type="text" value={address} onChange={handleAddressChange} placeholder="Address" />
      <input type="password" value={oldPassword} onChange={handleOldPasswordChange} placeholder="Current Password" required />
      <input type="password" value={password} onChange={handlePasswordChange} placeholder="New Password" />
      <button type="submit">Save</button>
    </form>
  );
}

function UserList({ users, onEdit }) {
  const [expandedUser, setExpandedUser] = useState(null);

  const handleViewDetail = (user) => {
    setExpandedUser(user);
  };

  const handleCloseDetail = () => {
    setExpandedUser(null);
  };

  return (
    <ul>
      {users.map((user) => (
        <li key={user.email}>
          <p>Email: {user.email}</p>
          <button onClick={() => onEdit(user)}>Edit</button>
          {expandedUser === user ? (
            <div>
              <p>First Name: {user.firstName}</p>
              <p>Last Name: {user.lastName}</p>
              <p>Phone: {user.phone}</p>
              <p>Address: {user.address}</p>
              <p>CCCD: {user.cccd}</p>
              <p>Socials - Facebook: {user.socialsFB}</p>
              <p>Socials - Twitter: {user.socialsTW}</p>
              <button onClick={handleCloseDetail}>Close Detail</button>
            </div>
          ) : (
            <button onClick={() => handleViewDetail(user)}>View Detail</button>
          )}
        </li>
      ))}
    </ul>
  );
}

export default UserForm;